# Pathway enrichment analysis

Pathway enrichment analysis using the COVID19 Disease Map, WikiPathways and Reactome.

The workflow is implemented in R in two steps - first you run the differential gene expression analysis script (1_DEGAnalysis) and then you perform the enrichment analysis (2_EnrichmentAnalysis).


### Requirements
* Workflow has been tested with R version 4.2.1.
* Make sure you have Cytoscape installed (version 3.9.1+) and running before you start running the script. 

### Data 
Transcriptomics data
* NHBE and A549 cells mock treated vs SARS-CoV-2 infected: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE147507
* Series 1 (NHBE) and series 5 (A549) were included.
* Blanco-Melo, Daniel, et al. "Imbalanced host response to SARS-CoV-2 drives development of COVID-19." Cell 181.5 (2020): 1036-1045.

Pathway information
* [COVID19 Disease Map](https://covid19map.elixir-luxembourg.org/) - version 2022-01-19 (generate by: https://github.com/BIGCAT-COVID19/COVID19-DM2GMT)
* [WikiPathways](https://wikipathways.org/) - release 2022-09-10
* [Reactome](https://reactome.org/) - MSigDb version 7.5.1

### Project team
* [Martina Kutmon](https://github.com/mkutmon)
* [Finterly Hu](https://github.com/Finterly)
* [Nhung Pham](https://github.com/nhungpham1707)
